# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 18:05:30 2017

@author: IN0120
"""

import __main__ as main
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
from datetime import datetime
from pathlib import Path
import copy
import math
import sys

import SaxUtil
import SaxHelper
import mEnums
from Util import printl
from Objects import SEQ_INFO_HOTSAX, SEQ_INFO_HOTTREE, SEQ_PROB_OBJECT
from StringBuilder import StringBuilder
import CsvColumns

FULL_DEBUG = False
FILE_DEBUG = True
READ_FROM_CONFIG_FILE = True
CONFIG_FILE_NAME = "_config/p1_final_data.ini"

##### SETTINGS ############################################################################
WINDOW_SIZE = 10000
PAA_SIZE = 10
ALPHABET_SIZE_SAT = 5
NUM_ANOMALY_HOTSAX = 40
SAT_OCCURRENCE_ANOMALY_CUTOFF = 1       #1 percent
USE_PERCENTAGE = False
Z_NORM_THRESHOLD = 0.01
TIME_STAMP_FORMAT = "%Y-%m-%d %H:%M:%S"
mSatCutType = mEnums.CutType.LINEAR_CUT
customSatCutArray = []
NUM_TOP_MOTIFS = 7
HAS_TIMESTAMP = True
HAS_HEADERS = None  # 0,None
USE_PAA_LIMITS = False

csvInFile = "C:/Users/in0120/Desktop/python test/201B_gbx_hspd_de_temp_60_WITH_TIME_DT-REMOVED.csv"
csvOutFile = ""
csvSeqDetailFile = ""
csvAnomalyFile = ""
directory = ""
fileName = ""
fileWoEx = ""

comma = ","
nl = "\n"

##### VARIABLES ############################################################################
startTime = 0
seg_size = 0
total_sequences = 0
maxConfidence = 0
minConfidence = sys.maxsize

df = pd.DataFrame
mTimeSeriesDateList = list()
mTimeSeriesValueList = list()
zNormArray = list()
mSatSubsList = list()
mHotSaxPaaArray = list()
nnArray = list()

seqInfoHotSaxArrayList = list()
seqInfoHotSaxArrayListNnSorted = list()

sat360SeqArray = list()
sat360ProbArray = list()

seqInfoHotTreeArrayList = list()

##### STRINGS ##############################################################################
underscore = "_"
csv = ".csv"
txt = ".txt"

##### FUNCTIONS ############################################################################

def readTimeSeries():
    global df
    global mTimeSeriesDateList
    global mTimeSeriesValueList
    if HAS_TIMESTAMP:
        TIME_SERIES_COLS = [0, 1]
        df = pd.read_csv(os.path.abspath(csvInFile), usecols=TIME_SERIES_COLS, names=['ts', 'value'], engine='python',
                         header=HAS_HEADERS)
        mTimeSeriesDateList = df['ts'].values.tolist()
        mTimeSeriesValueList = df['value'].values.tolist()
    else:
        TIME_SERIES_COLS = [0]
        df = pd.read_csv(os.path.abspath(csvInFile), usecols=TIME_SERIES_COLS, names=['value'], engine='python',
                         header=HAS_HEADERS)
        mTimeSeriesValueList = df['value'].values.tolist()
        
def getVariables():
    global mSatCutType, ALPHABET_SIZE_SAT, customSatCutArray
    if(mSatCutType == mEnums.CutType.CUSTOM_CUT):
        ALPHABET_SIZE_SAT = len(customSatCutArray)+1
    
    global csvInFile, csvOutFile, directory, fileName, fileWoEx, underscore
    p = Path(csvInFile)
    directory = str(p.parents[0])+"\\"
    fileName = p.name
    fileWoEx = os.path.splitext(fileName)[0]
    csvOutFile = directory + fileWoEx + underscore + "W-" + str(WINDOW_SIZE) + underscore + "P-" + str(PAA_SIZE) + underscore + "a-SAT-" + str(ALPHABET_SIZE_SAT)

def getVal(c):
    return ord(c) - 96

def findSubIndex(sax_sub, stage):
    global PAA_SIZE, ALPHABET_SIZE_SAT
    if (stage == PAA_SIZE): return int(getVal(sax_sub[stage - 1]))
    else :
        prepoints = int((math.pow(ALPHABET_SIZE_SAT, PAA_SIZE - stage)) * (getVal(sax_sub[stage - 1]) - 1))
        return prepoints + findSubIndex(sax_sub, stage + 1)
    
def getProbPercentage(pr):
    global total_sequences
    return ((pr * 100) / total_sequences)


##### READ PARAMETERS ############################################################################

if len(sys.argv)>1:
    printl("############### PARAMETERS ###############")
    printl("Reading from GUI")

    csvInFile = sys.argv[1]
    printl("File Path: %s" %(csvInFile))
    WINDOW_SIZE = int(sys.argv[2])
    printl("Window Size: %d" %(WINDOW_SIZE))
    PAA_SIZE = int(sys.argv[3])
    printl("PAA Size: %d" %(PAA_SIZE))
    ALPHABET_SIZE_SAT = int(sys.argv[4])
    printl("Alphabet Size: %d" %(ALPHABET_SIZE_SAT))
    NUM_ANOMALY_HOTSAX = int(sys.argv[5])
    printl("HOTSAX anomaly display limit: %d" %(NUM_ANOMALY_HOTSAX))
    SAT_OCCURRENCE_ANOMALY_CUTOFF = float(sys.argv[6])
    printl("SAT occurrence anomaly cutoff: %f" %(SAT_OCCURRENCE_ANOMALY_CUTOFF))
    if sys.argv[7]=='True':
        USE_PERCENTAGE = True
    else:
        USE_PERCENTAGE = False
    printl("Use percentage: %s" %(USE_PERCENTAGE))
    if sys.argv[8] == "Linear":
        mSatCutType = mEnums.CutType.LINEAR_CUT
    elif sys.argv[8] == "Gaussian":
        mSatCutType = mEnums.CutType.GAUSSIAN_CUT
    elif sys.argv[8] == "Custom":
        mSatCutType = mEnums.CutType.CUSTOM_CUT
    printl("Breakpoint method: %s" %(mSatCutType.name))
    customSatCutArray = [float(num) for num in sys.argv[9].split(',')]
    printl("Custom breakpoint array: %s" %(str(customSatCutArray)))
    TIME_STAMP_FORMAT = sys.argv[10].replace('#',' ')
    printl("Timestamp format: %s" %(str(TIME_STAMP_FORMAT)))
    if sys.argv[11]=='True':
        HAS_TIMESTAMP = True
    else:
        HAS_TIMESTAMP = False
    printl("Has timestamps: %r" %(HAS_TIMESTAMP))
    if sys.argv[12]=='True':
        HAS_HEADERS = 0
    else:
        HAS_HEADERS = None
    printl("Has headers (0-True, None-False): %s" %str(HAS_HEADERS))


elif READ_FROM_CONFIG_FILE:
    printl("############### PARAMETERS ###############")
    printl("Reading from config file")
        
    lines = [line.rstrip('\n') for line in open(CONFIG_FILE_NAME)]
    
    for i in range(0, len(lines)):
        if i==0:
           csvInFile= str((lines[i].split('#', 1)[0].split('=', 1)[1])).strip()
           printl("File Path: %s" %(csvInFile))
        elif i==1:
            WINDOW_SIZE= int((lines[i].split('#', 1)[0].split('=', 1)[1]))
            printl("Window Size: %d" %(WINDOW_SIZE))
        elif i==2:
            PAA_SIZE= int((lines[i].split('#', 1)[0].split('=', 1)[1]))
            printl("PAA Size: %d" %(PAA_SIZE))
        elif i==3:
            ALPHABET_SIZE_SAT= int((lines[i].split('#', 1)[0].split('=', 1)[1]))
            printl("Alphabet Size: %d" %(ALPHABET_SIZE_SAT))
        elif i==4:
            NUM_ANOMALY_HOTSAX= int((lines[i].split('#', 1)[0].split('=', 1)[1]))
            printl("HOTSAX anomaly display limit: %d" %(NUM_ANOMALY_HOTSAX))
        elif i==5:
            SAT_OCCURRENCE_ANOMALY_CUTOFF= float((lines[i].split('#', 1)[0].split('=', 1)[1]))
            printl("SAT occurrence anomaly cutoff: %f" %(SAT_OCCURRENCE_ANOMALY_CUTOFF))
        elif i==6:
            temp = str((lines[i].split('#', 1)[0].split('=', 1)[1])).strip()
            print(temp)
            if temp == "Linear":
                mSatCutType = mEnums.CutType.LINEAR_CUT
            elif temp == "Gaussian":
                mSatCutType = mEnums.CutType.GAUSSIAN_CUT
            elif temp == "Custom":
                mSatCutType = mEnums.CutType.CUSTOM_CUT
            printl("Breakpoint method: %s" %(mSatCutType.name))
        elif i==7:
            temp= str((lines[i].split('#', 1)[0].split('=', 1)[1])).split(',')
            customSatCutArray = [float(num) for num in temp]
            printl("Custom breakpoint array: %s" %(str(customSatCutArray)))
        elif i==8:
            temp= str((lines[i].split('#', 1)[0].split('=', 1)[1])).strip()
            if temp == "True":
                HAS_TIMESTAMP = True
            elif temp == "False":
                HAS_TIMESTAMP = False
            printl("Has timestamps: %r" %(HAS_TIMESTAMP))
        elif i==9:
            temp= str((lines[i].split('#', 1)[0].split('=', 1)[1])).strip()
            if temp == "True":
                HAS_HEADERS = 0
            elif temp == "False":
                HAS_HEADERS = None
            printl("Has headers (0-True, None-False): %s" %str(HAS_HEADERS))

else:
    printl("############### INPUT PARAMETERS ###############")
    printl(">>>>>>>> PRESS ENTER TO SKIP PARAMETER <<<<<<<<<")
    
    tempIn = input('Enter file path: \n>>')
    if tempIn:
        csvInFile=tempIn
    print("File Path: %s" %(csvInFile))
    
    tempIn = input('Enter window size (default - %d): \n>>' %WINDOW_SIZE)
    if tempIn:
        WINDOW_SIZE=int(tempIn)
    print("Window Size: %d" %(WINDOW_SIZE))
    
    tempIn = input('Enter paa size (default - %d): \n>>' %PAA_SIZE)
    if tempIn:
        PAA_SIZE=int(tempIn)
    print("PAA Size: %d" %(PAA_SIZE))
    
    tempIn = input('Enter alphabet size (default - %d): \n>>' %ALPHABET_SIZE_SAT)
    if tempIn:
        ALPHABET_SIZE_SAT=int(tempIn)
    print("Alphabet Size: %d" %(ALPHABET_SIZE_SAT))
    
    tempIn = input('Enter HOTSAX anomaly display limit (default - %d): \n>>' %NUM_ANOMALY_HOTSAX)
    if tempIn:
        NUM_ANOMALY_HOTSAX=int(tempIn)
    print("HOTSAX anomaly display limit: %d" %(NUM_ANOMALY_HOTSAX))
    
    tempIn = input('Enter SAT occurrence cutoff (default - %f): \n>>' %SAT_OCCURRENCE_ANOMALY_CUTOFF)
    if tempIn:
        SAT_OCCURRENCE_ANOMALY_CUTOFF=float(tempIn)
    print("SAT occurrence anomaly cutoff: %f" %(SAT_OCCURRENCE_ANOMALY_CUTOFF))
    
    tempIn = input('Enter breakpoint method [Linear/Gaussian/Custom] (default - Linear): \n>>')
    if tempIn:
        if tempIn == "Linear":
            mSatCutType = mEnums.CutType.LINEAR_CUT
        elif tempIn == "Gaussian":
            mSatCutType = mEnums.CutType.GAUSSIAN_CUT
        elif tempIn == "Custom":
            mSatCutType = mEnums.CutType.CUSTOM_CUT
    print("Breakpoint method: %s" %(mSatCutType.name))
    
    tempIn = input('Enter custom breakpoint array (a1,a2,a3): \n>>')
    if tempIn:
        tempIn = tempIn.split(',')
        customSatCutArray = [float(num) for num in tempIn]
    print("Custom breakpoint array: %s" %(str(customSatCutArray)))
    
    tempIn = input('Has timestamps? [True/False] (default - %r): \n>>' %HAS_TIMESTAMP)
    if tempIn:
        if tempIn == "True":
            HAS_TIMESTAMP = True
        elif tempIn == "False":
            HAS_TIMESTAMP = False
    print("Has timestamps: %r" %(HAS_TIMESTAMP))
    
    tempIn = input('Has headers? [True/False] (default - False): \n>>')
    if tempIn:
        if tempIn == "True":
            HAS_HEADERS = 0
        elif tempIn == "False":
            HAS_HEADERS = None
    print("Has headers (0-True, None-False): %s" %str(HAS_HEADERS))

##### SAX ############################################################################
printl("############### HOTTREE LOG ###############")
startTime = datetime.now()

#ClearVariables
getVariables()

readTimeSeries()
print("Input points: %s" %(len(df)))

#Z-Norm && Get Cuts Array
print("Segment Type: %s" %(mSatCutType.name))
if (mSatCutType == mEnums.CutType.GAUSSIAN_CUT) :  
    zNormArray = SaxUtil.znorm(mTimeSeriesValueList, Z_NORM_THRESHOLD)
else:
    zNormArray = mTimeSeriesValueList[:]
    
#PAA Array || SAX String
char_length = WINDOW_SIZE / PAA_SIZE   #PAA String Length
print("Input char length: %f" %(char_length))
paaArray = SaxHelper.generatePaaArray(zNormArray, int(round(len(mTimeSeriesValueList) / char_length)))
print("PAA Array Length: %d" %(len(paaArray)))

if USE_PAA_LIMITS:
    paaLimits = list()
    paaLimits.append(min(paaArray))
    paaLimits.append(max(paaArray))
else:
    paaLimits = []

satCutsArray = SaxUtil.generateCuts(zNormArray, paaLimits, mSatCutType, ALPHABET_SIZE_SAT, customSatCutArray)
printl("Breakpoints")
print(satCutsArray)

satString = SaxHelper.timeSeriesToString(paaArray, satCutsArray)
#print(satString)
printl("SAT String Length: %d" %(len(satString)))

if(FULL_DEBUG or FILE_DEBUG):
    #PAA ARRAY
    paafile = open(csvOutFile+"_PAA_PY"+txt, 'w')
    for item in paaArray:
        paafile.write("%f\n" % item)
    paafile.close()
    
    #SAX STRING
    saxstringfile = open(csvOutFile+"_SAX_STRING_PY"+txt, 'w')
    saxstringfile.write("%s" % (''.join(satString)))
    saxstringfile.close()
    
#Generate Subsequent Array
mSatSubsList = list()
mHotSaxPaaArray = list()
for i in range(0,len(paaArray) - PAA_SIZE+1):
    mHotSaxPaaArray.append(paaArray[i:i+PAA_SIZE])
    mSatSubsList.append(satString[i:i+PAA_SIZE])
    
total_sequences = len(mSatSubsList)
print("No. of SAT String Sequences : %d" % (len(mSatSubsList)))
print("No. of HOTSAX PAA Array Sequences : %d" % (len(mHotSaxPaaArray)))
    
##### HOTSAX ############################################################################    

#Generate NN Array
nnArray = list()
for i in range(0,len(paaArray) - (2 * (PAA_SIZE - 1))):
    nnArray.append(SaxHelper.getNnDistPaa(mHotSaxPaaArray[i], mHotSaxPaaArray[i + (PAA_SIZE - 1)]))

print("NN Array Length: %d" %(len(nnArray)))

#Create Time Stamps
seg_size = len(mTimeSeriesValueList) / len(paaArray)
print("Actual char length: %f" %(seg_size))
start_index = 0
timeStampArray = list()
posArray = list()
for i in range(0,len(paaArray)):
    if (HAS_TIMESTAMP): timeStampArray.append((mTimeSeriesDateList[start_index]))
    posArray.append(start_index)
    start_index = int(start_index+seg_size)
  
#Create Anomaly List (Pos sorted & NN sorted)    
seqInfoHotSaxArrayList = list()
seqInfoHotSaxArrayListNnSorted = list()
for i in range(0, len(nnArray)):
    if (HAS_TIMESTAMP): seqInfoHotSaxArrayList.append(SEQ_INFO_HOTSAX(i, posArray[i], timeStampArray[i], nnArray[i], WINDOW_SIZE))
    else: seqInfoHotSaxArrayList.append(SEQ_INFO_HOTSAX(i, posArray[i], 'NA', nnArray[i], WINDOW_SIZE))

seqInfoHotSaxArrayListNnSorted = copy.deepcopy(seqInfoHotSaxArrayList)
seqInfoHotSaxArrayListNnSorted.sort(key=lambda x: x.getNn(), reverse=True)


##### SAT ############################################################################

probList = list()
subIndexList = list()
subIndex = 0
for i, aMSatSubsList in enumerate(mSatSubsList):
    subIndex = findSubIndex(aMSatSubsList, 1)
    if(not(subIndex in subIndexList)):
        subIndexList.append(subIndex)
        probList.append(SEQ_PROB_OBJECT(subIndex, aMSatSubsList, 1))
    else:
        tempIndex = subIndexList.index(subIndex)
        tempProb = probList[tempIndex].getProb()
        probList[tempIndex].setProb(tempProb+1)

#Sort probability list high to low
probList.sort(key=lambda x: x.getProb(), reverse=True)

printl("Top Motifs: ")
if(NUM_TOP_MOTIFS>len(probList)): NUM_TOP_MOTIFS = len(probList)
for i in range(0,NUM_TOP_MOTIFS):
    print(''.join(probList[i].getSeq()))
    
sat360SeqArray = [None]*(len(mSatSubsList))
sat360ProbArray = [None]*(len(mSatSubsList))

seq = ""
prob = 0
for j, seqProbObject in enumerate(probList):
    seq = seqProbObject.getSeq()
    prob = seqProbObject.getProb()
    for i in range(0,len(mSatSubsList)):
        if (mSatSubsList[i]==seq):
            sat360SeqArray[i] = seq
            sat360ProbArray[i] = prob

##### HOTTREE ############################################################################

printl("Sequence occurrence cutoff: %f %%" %(SAT_OCCURRENCE_ANOMALY_CUTOFF))

seqInfoHotTreeArrayList = list()
for i in range(0, len(sat360ProbArray)):
    if USE_PERCENTAGE: satOccur = getProbPercentage(sat360ProbArray[i])
    else: satOccur = sat360ProbArray[i]
    
    if ((satOccur <= SAT_OCCURRENCE_ANOMALY_CUTOFF) and (i < len(seqInfoHotSaxArrayListNnSorted))):
        seq_info_hotsax = seqInfoHotSaxArrayList[i]
        con = seq_info_hotsax.getNn() / sat360ProbArray[i]
        if (con > maxConfidence): maxConfidence = con
        if (con < minConfidence): minConfidence = con
        seqInfoHotTreeArrayList.append(SEQ_INFO_HOTTREE(i, seq_info_hotsax.getPosition(), seq_info_hotsax.getDate(), con, seq_info_hotsax.getNn(), sat360ProbArray[i], getProbPercentage(sat360ProbArray[i]), sat360SeqArray[i], seq_info_hotsax.getLength()))


##### WRITE TO FILE ############################################################################
    
#Sequence Detail File
seqDetailFileName = csvOutFile+"_SEQ_DETAIL_PY"+csv
seqDetailFile = open(seqDetailFileName, 'w')
sb1 = StringBuilder()

for i in range(0, len(CsvColumns.columnsCsvFile)):
    sb1.Append(CsvColumns.columnsCsvFile[i])

for i, seqInfoNnSor in enumerate(seqInfoHotSaxArrayListNnSorted):
    seqIndex = seqInfoNnSor.getIndex()
    sb1.Append(nl)
    sb1.Append(seqIndex)
    sb1.Append(comma)
    sb1.Append(seqInfoNnSor.getPosition())
    sb1.Append(comma)
    sb1.Append(seqInfoNnSor.getDate())
    sb1.Append(comma)
    sb1.Append(seqInfoNnSor.getNn()/sat360ProbArray[seqIndex])
    sb1.Append(comma)
    sb1.Append(seqInfoNnSor.getNn())
    sb1.Append(comma)
    sb1.Append(sat360ProbArray[seqIndex])
    sb1.Append(comma)
    sb1.Append(getProbPercentage(sat360ProbArray[seqIndex]))
    sb1.Append(comma)
    sb1.Append(''.join(sat360SeqArray[seqIndex]))
    sb1.Append(comma)
    sb1.Append(WINDOW_SIZE)
    
seqDetailFile.write(str(sb1))
seqDetailFile.close()
printl("Sequence Detail File saved to: %s" %(seqDetailFileName))

#Anomaly Detail File
anomalyDetailFileName = csvOutFile+"_ANOMALY_DETAIL_PY"+csv
anomalyDetailFile = open(anomalyDetailFileName, 'w')
sb2 = StringBuilder()

for i in range(0, len(CsvColumns.columnsCsvFile)):
    sb2.Append(CsvColumns.columnsCsvFile[i])

for i, seqInfoHt in enumerate(seqInfoHotTreeArrayList):
    sb2.Append(nl)
    sb2.Append(seqInfoHt.getIndex)
    sb2.Append(comma)
    sb2.Append(seqInfoHt.getPosition())
    sb2.Append(comma)
    sb2.Append(seqInfoHt.getDate())
    sb2.Append(comma)
    sb2.Append(seqInfoHt.getConfidence())
    sb2.Append(comma)
    sb2.Append(seqInfoHt.getNn())
    sb2.Append(comma)
    sb2.Append(seqInfoHt.getProb())
    sb2.Append(comma)
    sb2.Append(seqInfoHt.getProbPer())
    sb2.Append(comma)
    sb2.Append(''.join(seqInfoHt.getSeq()))
    sb2.Append(comma)
    sb2.Append(seqInfoHt.getLength())
    
anomalyDetailFile.write(str(sb2))
anomalyDetailFile.close()
printl("Anomaly Detail File saved to: %s" %(anomalyDetailFileName))

##### CLEANUP ############################################################################
printl("############### END ###############")
executionTime = datetime.now() - startTime
printl ("Execution time: %s" %str(executionTime))

##### PLOT ############################################################################

plotStartTime = datetime.now()
if HAS_TIMESTAMP:
    df.index = pd.to_datetime(df['ts'], format = TIME_STAMP_FORMAT)
    del df['ts']
    df = pd.Series(data = df['value'], index = df.index, dtype = 'float32')

    fig = plt.gcf()
    fig.canvas.set_window_title(fileWoEx)

    ax1 = plt.subplot(311)
    plt.plot(df, linewidth=0.5, color="g")
    plt.title("HOTSAX Anomalies. Anomaly limit: %d" %(NUM_ANOMALY_HOTSAX))
    #plt.xlabel("Time")
    #plt.ylabel("Temperature")
    limitedSeqInfoHotsax = seqInfoHotSaxArrayListNnSorted[0:NUM_ANOMALY_HOTSAX] 
    for i, anomaly in enumerate(limitedSeqInfoHotsax):
        plt.axvspan(*mdates.datestr2num([mTimeSeriesDateList[anomaly.getPosition()], mTimeSeriesDateList[anomaly.getPosition()+anomaly.getLength()]]), color='red', alpha=1)

    ax2 = plt.subplot(312, sharex=ax1)
    plt.plot(df, linewidth=0.5, color="b")
    plt.title("SAT Anomalies. Anomaly limit: Occurrence = %f" %(SAT_OCCURRENCE_ANOMALY_CUTOFF))
    #plt.xlabel("Time")
    #plt.ylabel("Temperature")
    for i, anomaly in enumerate(seqInfoHotTreeArrayList):
        plt.axvspan(*mdates.datestr2num([mTimeSeriesDateList[anomaly.getPosition()], mTimeSeriesDateList[anomaly.getPosition()+anomaly.getLength()]]), color='red', alpha=1)

    ax3 = plt.subplot(313, sharex=ax1)
    plt.plot(df, linewidth=0.5, color="k")
    plt.title("HOTTREE Anomalies")
    #plt.xlabel("Time")
    #plt.ylabel("Temperature")
    gradientSeqInfoHottree = seqInfoHotTreeArrayList[:]
    gradientSeqInfoHottree.sort(key=lambda x: x.getConfidence(), reverse=True)
    for i, anomaly in enumerate(gradientSeqInfoHottree):
        alpha = (anomaly.getConfidence() - minConfidence) / (maxConfidence - minConfidence)
        plt.axvspan(*mdates.datestr2num([mTimeSeriesDateList[anomaly.getPosition()], mTimeSeriesDateList[anomaly.getPosition()+anomaly.getLength()]]), color='red', alpha=alpha)
    
    plt.tight_layout()
    if hasattr(main, '__file__'): plt.show()

else:
    df = pd.Series(data = df['value'], index = df.index, dtype = 'float32')

    fig = plt.gcf()
    fig.canvas.set_window_title(fileWoEx)

    ax1 = plt.subplot(311)
    plt.plot(df, linewidth=0.5, color="g")
    plt.title("HOTSAX Anomalies. Anomaly limit: %d" %(NUM_ANOMALY_HOTSAX))
    #plt.xlabel("Time")
    #plt.ylabel("Temperature")
    limitedSeqInfoHotsax = seqInfoHotSaxArrayListNnSorted[0:NUM_ANOMALY_HOTSAX] 
    for i, anomaly in enumerate(limitedSeqInfoHotsax):
        plt.axvspan(anomaly.getPosition(), anomaly.getPosition()+anomaly.getLength(), color='red', alpha=1)

    ax2 = plt.subplot(312, sharex=ax1)
    plt.plot(df, linewidth=0.5, color="b")
    plt.title("SAT Anomalies. Anomaly limit: Occurrence = %f" %(SAT_OCCURRENCE_ANOMALY_CUTOFF))
    #plt.xlabel("Time")
    #plt.ylabel("Temperature")
    for i, anomaly in enumerate(seqInfoHotTreeArrayList):
        plt.axvspan(anomaly.getPosition(), anomaly.getPosition()+anomaly.getLength(), color='red', alpha=1)

    ax3 = plt.subplot(313, sharex=ax1)
    plt.plot(df, linewidth=0.5, color="k")
    plt.title("HOTTREE Anomalies")
    #plt.xlabel("Time")
    #plt.ylabel("Temperature")
    gradientSeqInfoHottree = seqInfoHotTreeArrayList[:]
    gradientSeqInfoHottree.sort(key=lambda x: x.getConfidence(), reverse=True)
    for i, anomaly in enumerate(gradientSeqInfoHottree):
        alpha = (anomaly.getConfidence() - minConfidence) / (maxConfidence - minConfidence)
        plt.axvspan(anomaly.getPosition(), anomaly.getPosition()+anomaly.getLength(), color='red', alpha=alpha)
    
    plt.tight_layout()
    if hasattr(main, '__file__'): plt.show()
    
plotTime = datetime.now() - plotStartTime
printl ("Plot time: %s" %str(plotTime))

printl("###################################")