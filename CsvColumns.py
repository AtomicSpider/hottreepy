# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 16:08:12 2017

@author: IN0120
"""

comma = ","
columnsCsvFile = list()

columnsCsvFile.append("Sequence Index")
columnsCsvFile.append(comma)
columnsCsvFile.append("Position")
columnsCsvFile.append(comma)
columnsCsvFile.append("Window Start Date")
columnsCsvFile.append(comma)
columnsCsvFile.append("HOTTREE CONFIDENCE")
columnsCsvFile.append(comma)
columnsCsvFile.append("HOTSAX NN Distance")
columnsCsvFile.append(comma)
columnsCsvFile.append("Sequence Occurrence")
columnsCsvFile.append(comma)
columnsCsvFile.append("SAT Probability %")
columnsCsvFile.append(comma)
columnsCsvFile.append("SAT Sequence")
columnsCsvFile.append(comma)
columnsCsvFile.append("Window Length")