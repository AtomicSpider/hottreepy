# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 12:06:34 2017

@author: IN0120
"""

import math

def generatePaaArray(ts, paaSize):
    # fix the length
    length = len(ts)
    if (length < paaSize):
        print("PAA size can't be greater than the timeseries size.")
        return
        
    # check for the trivial case
    if (length == paaSize):
        return ts
    else:
        paa = list()
        breaks = list()
        pointsPerSegment = length / paaSize
        for i in range(0, paaSize+1):
            breaks.append(i * pointsPerSegment)

        for i in range(0, paaSize):
            segStart = breaks[i]
            segEnd = breaks[i + 1]

            fractionStart = math.ceil(segStart) - segStart
            fractionEnd = segEnd - math.floor(segEnd)

            fullStart = math.floor(segStart)
            fullEnd = math.ceil(segEnd)
            
            segment = ts[fullStart:fullEnd]

            if (fractionStart > 0):
                segment[0] = segment[0] * fractionStart

            if (fractionEnd > 0):
                segment[len(segment) - 1] = segment[len(segment) - 1] * fractionEnd

            elementsSum = 0
            for i, e in enumerate(segment):
                elementsSum = elementsSum + e

            paa.append(elementsSum / pointsPerSegment)

        return paa

def num2char(value, cuts):
    ALPHABET = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    count = 0
    while ((count < len(cuts)) and (cuts[count] <= value)):
        count+=1
    return ALPHABET[count]
    
def timeSeriesToString(vals, cuts):
    res = list()
    for i in range(0,len(vals)):
        res.append(num2char(vals[i], cuts))
    return res

def getNnDistPaa(a, b):
    sum = 0
    for i in range(0,len(a)):
        sum += math.pow((b[i] - a[i]), 2)
    return math.sqrt(sum)