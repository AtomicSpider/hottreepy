# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 18:04:50 2017

@author: IN0120
"""

from enum import Enum
    
CutType = Enum('CutType', 'GAUSSIAN_CUT LINEAR_CUT CUSTOM_CUT')

MethodType = Enum('MethodType', 'HOTSAX SAT')