# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 16:57:32 2017

@author: IN0120
"""

import tkinter as tk
from tkinter import ttk, filedialog
import os

root = tk.Tk()
root.title("Hottree")
csvInFile = tk.StringVar()

def defArg(input):
	if input=='':
		return 'NA'
	else:
		return input

def browseCsvInFile(*args):
    filename = filedialog.askopenfilename(initialdir = os.getcwd(),title = "Browse Time Series",filetypes = [("CSV files","*.csv")])
    #print(filename)
    csvInFile_entry.insert(0,str(filename))
    
    
def ExtractParameters(*args):
    print("Run: \"%s\" %d %d %d %d %f %r %s %s %s %r %r" %(defArg(csvInFile.get()), int(WINDOW_SIZE.get()), int(PAA_SIZE.get()), int(ALPHABET_SIZE_SAT.get()), int(NUM_ANOMALY_HOTSAX.get()), float(SAT_OCCURRENCE_ANOMALY_CUTOFF.get()), USE_PERCENTAGE.get(), defArg(mSatCutType.get()), defArg(customSatCutArray.get()), defArg(timeStampFormat.get()), HAS_TIMESTAMP.get(), HAS_HEADERS.get()))
    os.system("python HotTreePy.py \"%s\" %d %d %d %d %f %r %s %s %s %r %r" %(defArg(csvInFile.get()), int(WINDOW_SIZE.get()), int(PAA_SIZE.get()), int(ALPHABET_SIZE_SAT.get()), int(NUM_ANOMALY_HOTSAX.get()), float(SAT_OCCURRENCE_ANOMALY_CUTOFF.get()), USE_PERCENTAGE.get(), defArg(mSatCutType.get()), defArg(customSatCutArray.get()), defArg(timeStampFormat.get().replace(' ','#')), HAS_TIMESTAMP.get(), HAS_HEADERS.get()))

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)


ttk.Label(mainframe, text="Input File").grid(column=1, row=1, sticky=tk.W)
csvInFile_entry = ttk.Entry(mainframe, width=80, textvariable=csvInFile)
csvInFile_entry.grid(column=2, row=1, sticky=tk.W)
csvInFile_entry.insert(0, "")
ttk.Button(mainframe, text="Browse", command=browseCsvInFile).grid(column=3, row=1, sticky=tk.W)

WINDOW_SIZE = tk.StringVar()
ttk.Label(mainframe, text="Window Size").grid(column=1, row=2, sticky=tk.W)
WINDOW_SIZE_entry = ttk.Entry(mainframe, width=25, textvariable=WINDOW_SIZE)
WINDOW_SIZE_entry.grid(column=2, row=2, sticky=tk.W)
WINDOW_SIZE_entry.insert(0, "145")

PAA_SIZE = tk.StringVar()
ttk.Label(mainframe, text="PAA Size").grid(column=1, row=3, sticky=tk.W)
PAA_SIZE_entry = ttk.Entry(mainframe, width=25, textvariable=PAA_SIZE)
PAA_SIZE_entry.grid(column=2, row=3, sticky=tk.W)
PAA_SIZE_entry.insert(0, "8")

ALPHABET_SIZE_SAT = tk.StringVar()
ttk.Label(mainframe, text="Alphabet Size").grid(column=1, row=4, sticky=tk.W)
ALPHABET_SIZE_SAT_entry = ttk.Entry(mainframe, width=25, textvariable=ALPHABET_SIZE_SAT)
ALPHABET_SIZE_SAT_entry.grid(column=2, row=4, sticky=tk.W)
ALPHABET_SIZE_SAT_entry.insert(0, "5")

NUM_ANOMALY_HOTSAX = tk.StringVar()
ttk.Label(mainframe, text="Hotsax plot anomaly limit").grid(column=1, row=5, sticky=tk.W)
NUM_ANOMALY_HOTSAX_entry = ttk.Entry(mainframe, width=25, textvariable=NUM_ANOMALY_HOTSAX)
NUM_ANOMALY_HOTSAX_entry.grid(column=2, row=5, sticky=tk.W)
NUM_ANOMALY_HOTSAX_entry.insert(0, "20")

SAT_OCCURRENCE_ANOMALY_CUTOFF = tk.StringVar()
ttk.Label(mainframe, text="SAT Occurrence Cutoff").grid(column=1, row=6, sticky=tk.W)
SAT_OCCURRENCE_ANOMALY_CUTOFF_entry = ttk.Entry(mainframe, width=25, textvariable=SAT_OCCURRENCE_ANOMALY_CUTOFF)
SAT_OCCURRENCE_ANOMALY_CUTOFF_entry.grid(column=2, row=6, sticky=tk.W)
SAT_OCCURRENCE_ANOMALY_CUTOFF_entry.insert(0, "1")

USE_PERCENTAGE = tk.BooleanVar()
USE_PERCENTAGE_entry = tk.Checkbutton(mainframe, text="Use Percentage Occurrence?", variable=USE_PERCENTAGE, onvalue=True, offvalue=False)
USE_PERCENTAGE_entry.grid(column=3, row=6, sticky=tk.W)
USE_PERCENTAGE_entry.deselect()

mSatCutType = tk.StringVar()
ttk.Label(mainframe, text="Breakpoints Method (Linear, Gaussian, Custom)").grid(column=1, row=7, sticky=tk.W)
mSatCutType_entry = ttk.Entry(mainframe, width=25, textvariable=mSatCutType)
mSatCutType_entry.grid(column=2, row=7, sticky=tk.W)
mSatCutType_entry.insert(0, "Linear")

customSatCutArray = tk.StringVar()
ttk.Label(mainframe, text="Custom Breakpoint Array").grid(column=1, row=8, sticky=tk.W)
customSatCutArray_entry = ttk.Entry(mainframe, width=25, textvariable=customSatCutArray)
customSatCutArray_entry.grid(column=2, row=8, sticky=tk.W)
customSatCutArray_entry.insert(0, "1,2,3")

timeStampFormat = tk.StringVar()
ttk.Label(mainframe, text="Timestamp Format").grid(column=1, row=9, sticky=tk.W)
timeStampFormat_entry = ttk.Entry(mainframe, width=25, textvariable=timeStampFormat)
timeStampFormat_entry.grid(column=2, row=9, sticky=tk.W)
timeStampFormat_entry.insert(0, "%Y-%m-%d %H:%M:%S")

HAS_TIMESTAMP = tk.BooleanVar()
HAS_TIMESTAMP_entry = tk.Checkbutton(mainframe, text="Has Timestamps?", variable=HAS_TIMESTAMP, onvalue=True, offvalue=False)
HAS_TIMESTAMP_entry.grid(column=3, row=9, sticky=tk.W)
HAS_TIMESTAMP_entry.select()

HAS_HEADERS = tk.BooleanVar()
HAS_HEADERS_entry = tk.Checkbutton(mainframe, text="Has Headers?", variable=HAS_HEADERS, onvalue=True, offvalue=False)
HAS_HEADERS_entry.grid(column=2, row=10, sticky=tk.W)
HAS_HEADERS_entry.deselect()

ttk.Button(mainframe,width=80,  text="RUN", command=ExtractParameters).grid(column=2, row=11, sticky=tk.W)


for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

csvInFile_entry.focus()

root.mainloop()