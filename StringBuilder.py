# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 16:02:38 2017

@author: IN0120
"""

from io import StringIO

class StringBuilder:
     _file_str = None

     def __init__(self):
         self._file_str = StringIO()

     def Append(self, object):
         self._file_str.write(str(object))

     def __str__(self):
         return self._file_str.getvalue()