# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 12:06:34 2017

@author: IN0120
"""

class SEQ_INFO_HOTSAX(object):
    index= 0        #int
    position = 0    #int
    date = ""       #String
    nn = 0          #double
    length = 0      #int

    def __init__(self, index, position, date, nn, length):
        self.index = index
        self.position = position
        self.date = date
        self.nn = nn
        self.length = length
    
    def _print(self):
        print("index: %d, position: %d, date: %s, nn: %f, length: %d" %(self.index,self.position,self.date,self.nn,self.length))
        
    def getIndex(self) :
        return self.index    

    def setIndex(self,index) :
        self.index = index    

    def getPosition(self) :
        return self.position    

    def setPosition(self,position) :
        self.position = position  

    def getDate(self) :
        return self.date    

    def setDate(self,date) :
        self.date = date    

    def getNn(self) :
        return self.nn    

    def setNn(self,nn) :
        self.nn = nn    

    def getLength(self) :
        return self.length    

    def setLength(self,length) :
        self.length = length
        

class SEQ_INFO_HOTTREE(object):
    index= 0        #int
    position = 0    #int
    date = ""       #String
    confidence = 0  #double      
    nn = 0          #double
    prob = 0        #int
    prob_per = 0    #double
    seq = 0         #String
    length = 0      #int

    def __init__(self, index, position, date, confidence, nn, prob, prob_per, seq, length):
        self.index = index
        self.position = position
        self.date = date
        self.confidence = confidence
        self.nn = nn
        self.prob = prob
        self.prob_per = prob_per
        self.seq = seq
        self.length = length
    
    def _print(self):
        print("index: %d, position: %d, date: %s, confidence: %f, nn: %f, prob: %d, prob_per: %f, sequence: %s, length: %d" %(self.index,self.position,self.date, self.confidence,self.nn, self.prob, self.prob_per, self.seq,self.length))
        
    def getIndex(self) :
        return self.index    

    def setIndex(self,index) :
        self.index = index    

    def getPosition(self) :
        return self.position    

    def setPosition(self,position) :
        self.position = position  

    def getDate(self) :
        return self.date    

    def setDate(self,date) :
        self.date = date    

    def getConfidence(self) :
        return self.confidence    

    def setConfidence(self,confidence) :
        self.confidence = confidence    

    def getNn(self) :
        return self.nn    

    def setNn(self,nn) :
        self.nn = nn  

    def getProb(self) :
        return self.prob    

    def setProb(self,prob) :
        self.prob = prob     

    def getProbPer(self) :
        return self.prob_per    

    def setProbPer(self,prob_per) :
        self.prob_per = prob_per     

    def getSeq(self) :
        return self.seq    

    def setSeq(self,seq) :
        self.seq = seq    

    def getLength(self) :
        return self.length    

    def setLength(self,length) :
        self.length = length          
        
        
class SEQ_PROB_OBJECT(object):
    index= 0        #int
    seq = ""        #String
    prob = 0        #int

    def __init__(self, index, seq, prob):
        self.index = index
        self.seq = seq
        self.prob = prob
    
    def _print(self):
        print("index: %d, seq: %s, prob: %d" %(self.index,self.seq,self.prob))
        
    def getIndex(self) :
        return self.index    

    def setIndex(self,index) :
        self.index = index     

    def getSeq(self) :
        return self.seq    

    def setSeq(self,seq) :
        self.seq = seq  

    def getProb(self) :
        return self.prob    

    def setProb(self,prob) :
        self.prob = prob  

    